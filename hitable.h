#pragma once
#include "globals.h"
#include "ray.h"
#include <algorithm>


class material;

struct hit_record {
  float t;
  vec3 p;
  vec3 normal;
  material *mat_ptr;
};

class hitable {
public:
  virtual bool hit(const ray &r, float t_min, float t_max, hit_record &rec) const = 0;
};

vec3 reflect(const vec3 &v, const vec3 &n) {
  return v - 2 * dot(v, n) * n;
}


float saturate(float a) {
  return std::min(std::max((float)a, (float)0), (float)1);
}

bool refract(const vec3 &v, const vec3 &n, float ni_over_nt, vec3 &refracted) {
  vec3 uv = unit_vector(v);
  float dt = dot(uv, n);
  float discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt * dt);
  if(discriminant > 0) {
    refracted = ni_over_nt * (uv - n * dt) - n * sqrt(discriminant);
    return true;
  }
  else
    return false;
}

float schlick(float cosine, float ref_idx) {
  float r0 = (1 - ref_idx) / (1 + ref_idx);
  r0 = r0 * r0;
  return r0 + (1 - r0) * pow((1 - cosine), 5);
}

float uschlick(float cosine, float r0) {
  return r0 + (1 - r0) * pow((1 - cosine), 5);
}

float f0_from_ref_idx(float ref_idx) {
  float f0 = (1.0 - ref_idx) / (1.0 + ref_idx);
  return f0 * f0;
}

vec3 random_in_unit_sphere() {
  vec3 p;
  do {
    p = 2.0 * vec3(RAND(), RAND(), RAND()) - vec3(1, 1, 1);
  } while(p.squared_length() >= 1.0);
  return p;
}

class material {
public:
  virtual bool scatter(const ray &r_in, const hit_record &rec, vec3 &attenuation, ray &scattered) const = 0;
};

class flat : public material {
public:
  flat(const vec3 &a) : albedo(a) {}
  virtual bool scatter(const ray &r_in, const hit_record &rec, vec3 &attenuation, ray &scattered) const {
    vec3 target = rec.normal + random_in_unit_sphere();
    scattered = ray(rec.p, target);
    attenuation = albedo;
    return true;
  }
  vec3 albedo;
};

class lambertian : public material {
public:
  lambertian(const vec3 &a, float ro) : albedo(a), roughness(ro) {}
  virtual bool scatter(const ray &r_in, const hit_record &rec, vec3 &attenuation, ray &scattered) const {
    float cosine = saturate(dot(unit_vector(rec.normal), (unit_vector(r_in.direction()) * -1.0f)));
    float fresnel = uschlick(cosine, 0.04);
    vec3 target;
    if(RAND() > fresnel) {
      target = rec.normal + random_in_unit_sphere();
    }
    else {
      vec3 reflected = reflect(r_in.direction(), rec.normal);
      target = reflected + (roughness * random_in_unit_sphere());
    }
    //scattered = ray(rec.p, target - rec.p);
    scattered = ray(rec.p, target);
    attenuation = albedo;
    return true;
  }

  vec3 albedo;
  float roughness;
};

class metal : public material {
public:
  metal(const vec3 &a, float f) : albedo(a) {
    if(f < 1)
      fuzz = f;
    else
      fuzz = 1;
  }
  virtual bool scatter(const ray &r_in, const hit_record &rec, vec3 &attenuation, ray &scattered) const {
    vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    scattered = ray(rec.p, reflected + fuzz * random_in_unit_sphere());
    attenuation = albedo;
    return (dot(scattered.direction(), rec.normal) > 0);
  }
  vec3 albedo;
  float fuzz;
};

class dielectric : public material {
public:
  dielectric(vec3 color, float ro, float ri) : color(color), ref_idx(ri) {
    if(ro < 1)
      roughness = ro;
    else
      roughness = 1;
  }
  virtual bool scatter(const ray &r_in, const hit_record &rec, vec3 &attenuation, ray &scattered) const {
    vec3 outward_normal;
    vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    float ni_over_nt;
    attenuation = color;
    vec3 refracted;
    float reflect_prob;
    float cosine;
    if(dot(r_in.direction(), rec.normal) > 0) {
      outward_normal = -rec.normal;
      ni_over_nt = ref_idx;
      cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
      //cosine = dot(unit_vector(r_in.direction()), unit_vector(rec.normal));
    }
    else {
      outward_normal = rec.normal;
      ni_over_nt = 1.0 / ref_idx;
      cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
      //cosine = -dot(unit_vector(r_in.direction()), unit_vector(rec.normal));
    }

    float fresnel = schlick(saturate(cosine), ref_idx);

    if(refract(r_in.direction(), outward_normal, ni_over_nt, refracted)) {
      reflect_prob = fresnel;
    }
    else {
      reflect_prob = 1.0;
    }

    if(RAND() < reflect_prob) {
      scattered = ray(rec.p, reflected + (roughness * random_in_unit_sphere()));
    }
    else {
      scattered = ray(rec.p, refracted + (roughness * random_in_unit_sphere()));
    }
    return true;
  }
  float ref_idx;
  float roughness;
  vec3 color;
};