#pragma once
#include "camera.h"
#include "hitable.h"
#include <map>

class args {
public:
  args(camera &cam, hitable *world, double range_min, double range_max, int ny, int nx, int ns, pixel **image) : cam(cam), world(world), range_min(range_min), range_max(range_max), ny(ny), nx(nx), ns(ns), image(image){};
  camera &cam;
  hitable *world;
  double range_min;
  double range_max;
  int ny;
  int nx;
  int ns;
  pixel **image;
};