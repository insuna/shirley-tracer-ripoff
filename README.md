# shirley-tracer-ripoff

Raycaster built by explicitly copying it from Peter Shirley's book "Ray Tracing
in One Weekend"

Scene of objects is shamelessly ripped off from https://github.com/termhn/rayn

![Render](./ultra.png)
