#include "args.h"
#include "camera.h"
#include "color.h"
#include "globals.h"
#include "hitable.h"
#include "hitablelist.h"
#include "ray.h"
#include "sphere.h"
#include "vec3.h"
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <mutex>
#include <random>
#include <thread>
#include <vector>

void trace(args *argus) {
    for(int j = (int)(argus->range_max) - 1; j >= (int)(argus->range_min); j--) {
        for(int i = 0; i < argus->nx; i++) {
            vec3 col(0, 0, 0);
            for(int s = 0; s < argus->ns; s++) {
                float u = float(i + RAND()) / float(argus->nx);
                float v = float(j + RAND()) / float(argus->ny);
                ray r = argus->cam.get_ray(u, v);
                vec3 p = r.point_at_parameter(2.0);
                col += color(r, argus->world, 0);
            }
            col /= float(argus->ns);
            col = vec3(powf(col[0], (1.0 / 2.2)), powf(col[1], (1.0 / 2.2)), powf(col[2], (1.0 / 2.2)));
            int ir = int(255.0 * col[0]);
            int ig = int(255.0 * col[1]);
            int ib = int(255.0 * col[2]);
            m.lock();
            argus->image[i][j].r = ir;
            argus->image[i][j].g = ig;
            argus->image[i][j].b = ib;
            m.unlock();
        }
    }
}

int main(int argc, char **argv) {
    int nx, ny, ns;
    int threads = std::thread::hardware_concurrency() - NEG_THREADS;
    if(argc == 4) {
        nx = std::stoi(argv[1]);
        ny = std::stoi(argv[2]);
        ns = std::stoi(argv[3]);
    }
    else if(argc == 5) {
        nx = std::stoi(argv[1]);
        ny = std::stoi(argv[2]);
        ns = std::stoi(argv[3]);
        threads = std::stoi(argv[4]);
    }
    else {
        nx = RES_X;
        ny = RES_Y;
        ns = SAMPLES;
    }
    pixel **image = (pixel **)malloc(sizeof(pixel) * nx * ny);
    for(int i = 0; i < nx; i++) {
        image[i] = (pixel *)malloc(sizeof(pixel) * ny);
    }

    std::cout << "P3\n"
              << nx << " " << ny << "\n255\n";
    std::vector<hitable *> *list = new std::vector<hitable *>();

    vec3 lookfrom(0, 0, 1);
    vec3 lookat(0, 0, -1);
    float dist_to_focus = (lookfrom - lookat).length();
    float aperture = 0.0;
    list->emplace_back(new sphere(vec3(-1, 0, -1), 0.45, new flat(vec3(0.7, 0.3, 0.4))));
    list->emplace_back(new sphere(vec3(0, -200.5, -1), 200, new lambertian(vec3(0.35, 0.3, 0.45), 0.2)));
    list->emplace_back(new sphere(vec3(0, 0, -1), 0.5, new metal(vec3(0.9, 0.9, 0.9), 0.0))); //silver, regular value: 0.05
    list->emplace_back(new sphere(vec3(-1, 0, -1), 0.5, new dielectric(vec3(0.9, 0.9, 0.9), 0, 1.5)));
    list->emplace_back(new sphere(vec3(-0.5, -0.375, -0.5), 0.125, new metal(vec3(1.0, 0.9, 0.5), 0.2)));
    list->emplace_back(new sphere(vec3(1, -0.25, -1.0), 0.25, new metal(vec3(1.0, 0.9, 0.5), 0.0)));
    list->emplace_back(new sphere(vec3(0.4, -0.375, -0.5), 0.125, new dielectric(vec3(0.9, 0.9, 0.9), 0.0, 1.5)));
    list->emplace_back(new sphere(vec3(0.2, -0.4, -0.35), 0.1, new dielectric(vec3(0.9, 0.9, 0.9), 0.0, 1.5)));
    list->emplace_back(new sphere(vec3(0.5, -0.25, -0.25), 0.25, new dielectric(vec3(0.7, 0.3, 0.4), 0.005, 1.5)));   //wine glass sphere
    list->emplace_back(new sphere(vec3(-0.25, -0.375, -0.15), 0.125, new dielectric(vec3(0.9, 0.9, 0.9), 0.2, 1.5))); //rough glass sphere
    camera cam(lookfrom, lookat, vec3(0, 1, 0), 53, float(nx) / float(ny), aperture, dist_to_focus);
    hitable *world = new hitable_list(*list);

    std::vector<args *> *arguments = new std::vector<args *>();
    std::vector<std::thread *> *thread_group = new std::vector<std::thread *>();

    for(int i = 0; i < threads; i++) {
        arguments->emplace_back(new args(cam, world, ((double)((ny / threads) * i)), ((double)((ny / threads) * (i + 1))), ny, nx, ns, image));
    }

    for(args *a : *arguments) {
        thread_group->emplace_back(new std::thread(trace, a));
    }

    for(std::thread *t : *thread_group) {
        t->join();
    }


    for(int j = ny; j >= 0; j--) {
        for(int i = 0; i < nx; i++) {
            std::cout << image[i][j].r << " " << image[i][j].g << " " << image[i][j].b << "\n";
        }
    }

    for(int i = 0; i < nx; i++) {
        free(image[i]);
    }
    free(image);
}