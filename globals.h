#pragma once
#include <mutex>
#include <random>

#define MAX_BOUNCES 100
#define RES_X 1280
#define RES_Y 720
#define SAMPLES 1
#define NEG_THREADS 1

#define RAND() (double)rd() / (double)rd.max()

std::random_device rd;

typedef struct pixel {
  int r;
  int g;
  int b;
} pixel;

//int reflected_counter = 0;
//int refracted_counter = 0;

std::mutex m;
