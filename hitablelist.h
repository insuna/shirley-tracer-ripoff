#pragma once
#include "hitable.h"
#include "ray.h"
#include <vector>

class hitable_list : public hitable {
public:
  hitable_list() {}
  hitable_list(std::vector<hitable *> l) {
    list = l;
  }
  virtual bool hit(const ray &r, float tmin, float tmax, hit_record &rec) const;
  std::vector<hitable *> list;
  int list_size;
};

bool hitable_list::hit(const ray &r, float t_min, float t_max, hit_record &rec) const {
  hit_record temp_rec;
  bool hit_anything = false;
  double closest_so_far = t_max;
  for(auto *ht : list) {
    if(ht->hit(r, t_min, closest_so_far, temp_rec)) {
      hit_anything = true;
      closest_so_far = temp_rec.t;
      rec = temp_rec;
    }
  }
  return hit_anything;
}